import os
import time
import subprocess

def cmd(cmd):
    process = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()
    return output

gitCheckRemote = "git status"
gitPullRemote = "git pull"
gitFetchRemote = "git fetch"
closeProject = "~/pyttern/pyttern/scripts/projects/docker.sh -c down -p verre_tech"
startProject = "~/pyttern/pyttern/scripts/projects/docker.sh -c up -p verre_tech"

while True:
    output = ""
    output = cmd(gitFetchRemote)
    output = cmd(gitCheckRemote)
    output = str(output)

    if output.find("est en retard") > 0:
        print("See changes")
        output = cmd(gitPullRemote)
        print(output)
        os.system(closeProject)
        os.system(startProject)
    else:
        print('See no changes')

    time.sleep(10.0)
