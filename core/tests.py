from django.test import TestCase
from core.models import Product, Category


class CategoryTestCase(TestCase):
    def setUp(self):
        Category.objects.create(name="Categorie")

    def test_category_creation(self):
        categorie = Category.objects.get(name="Categorie")
        self.assertEqual("Categorie", categorie.name)


class ProductTestCase(TestCase):
    def setUp(self):
        Category.objects.create(name="Categorie")
        categorie = Category.objects.get(name="Categorie")
        Product.objects.create(name="verre", price=32, discount_price=30, category=categorie, description="text", image="1")

    def test_product_name(self):
        verre = Product.objects.get(name="verre")
        self.assertEqual("verre", verre.name)

    def test_product_price(self):
        verre = Product.objects.get(name="verre")
        self.assertEqual(32, verre.price)

    def test_product_discount_price(self):
        verre = Product.objects.get(name="verre")
        self.assertEqual(30, verre.discount_price)

    def test_product_description(self):
        verre = Product.objects.get(name="verre")
        self.assertEqual("text", verre.description)

    def test_product_image(self):
        verre = Product.objects.get(name="verre")
        self.assertEqual("1", verre.image)
