from django.conf.urls import url
from rest_framework import routers
from core.views import StoreViewSet, CategoryViewSet, ProductViewSet, StockViewSet, OrderViewSet

router = routers.DefaultRouter()
router.register(r'store', StoreViewSet)
router.register(r'category', CategoryViewSet)
router.register(r'product', ProductViewSet)
router.register(r'stock', StockViewSet)
router.register(r'order', OrderViewSet)

urlpatterns = router.urls