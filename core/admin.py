from django.contrib import admin
from .models import Store, Category, Product, Stock, GlobalOrder, Order

# Register your models here.
admin.site.register(Store)
admin.site.register(Category)
admin.site.register(Product)
admin.site.register(Stock)
admin.site.register(GlobalOrder)
admin.site.register(Order)