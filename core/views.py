from django.shortcuts import render
from .serializers import StoreSerializer, CategorySerializer, ProductSerializer, StockSerializer, OrderSerializer
from .models import Store, Category, Product, Stock, Order, GlobalOrder
from rest_framework import viewsets
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.permissions import IsAuthenticated
from django.template import loader
from django.http import HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect


def products(request):
    products = Product.objects.all()
    template = loader.get_template('core/products.html')
    context = {
        "products": products,
    }
    return HttpResponse(template.render(context, request))


def user(request):
    if request.user.is_authenticated:
        user = request.user
        template = loader.get_template('core/user.html')
        context = {
            "user": user,
        }
        return HttpResponse(template.render(context, request))
    return redirect('co')


def add_cart(request):
    print('toto')
    if request.user.is_authenticated:
        product = Product.objects.all().filter(pk=request.GET.get('id')).first()

        global_order = GlobalOrder.objects.all().filter(user=request.user.pk, ordered=False).first()
        if not global_order:
            global_order = GlobalOrder.objects.create(user=request.user)

        order = Order.objects.create(global_order=global_order, product=product, quantity=1)

        return redirect('products')
    return redirect('co')


def cart(request):
    if request.user.is_authenticated:
        global_order = GlobalOrder.objects.all().filter(user=request.user.pk, ordered=False).first()
        if not global_order:
            orders = Order.objects.all().filter(global_order=global_order)
            template = loader.get_template('core/cart.html')
            context = {
                "empty": True,
            }
            return HttpResponse(template.render(context, request))
        orders = Order.objects.all().filter(global_order=global_order)
        template = loader.get_template('core/cart.html')
        context = {
            "orders": orders,
        }
        return HttpResponse(template.render(context, request))
    return redirect('co')


def global_orders(request):
    if request.user.is_authenticated:
        global_orders_with_orders = {}
        global_orders = GlobalOrder.objects.all().filter(user=request.user.pk, ordered=True).order_by('-pk')
        for global_order in global_orders:
            orders = Order.objects.all().filter(global_order=global_order)
            ordered = global_order.ordered
            global_orders_with_orders[global_order.pk] = {
                'ordered': ordered,
                'orders': orders
            }

        template = loader.get_template('core/global_orders.html')
        print(global_orders_with_orders)
        context = {
            "global_orders": global_orders_with_orders,
        }
        return HttpResponse(template.render(context, request))
    return redirect('co')


def contact(request):
    if request.user.is_authenticated:
        pass
        # Do something for authenticated users.
    return redirect('co')


def co(request):
    if request.POST.get('username'):
        username = request.POST.get('username', False)
        password = request.POST.get('password', False)
        user = authenticate(username=username, password=password)
        if user is not None and user.is_active:
            login(request, user)
            return redirect('products')
        return redirect('co')
    template = loader.get_template('core/login.html')
    context = {}
    return HttpResponse(template.render(context, request))


def deco(request):
    logout(request)
    return redirect(products)


def valid_cart(request):
    if request.user.is_authenticated:
        global_orders = GlobalOrder.objects.all().filter(user=request.user.pk, ordered=False).update(ordered=True)
    return redirect('global_orders')


# Create your views here.
class StoreViewSet(viewsets.ModelViewSet):
    queryset = Store.objects.all()
    serializer_class = StoreSerializer

    authentication_classes = [JSONWebTokenAuthentication]
    permission_classes = [IsAuthenticated]


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class StockViewSet(viewsets.ModelViewSet):
    queryset = Stock.objects.all()
    serializer_class = StockSerializer


class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
