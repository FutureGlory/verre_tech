"""verre_tech URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from . import settings
from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from rest_framework_jwt.views import obtain_jwt_token
from core import views as coreviews
from django.contrib.staticfiles.urls import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    url('^$', coreviews.products, name="products"),
    url('user/', coreviews.user, name="user"),
    url('login/', coreviews.co, name="co"),
    url('logout/', coreviews.deco, name="deco"),
    url('add_cart/', coreviews.add_cart, name="add_cart"),
    url('valid_cart/', coreviews.valid_cart, name="valid_cart"),
    url('global_orders/', coreviews.global_orders, name="global_orders"),
    url('cart/', coreviews.cart, name="cart"),
    path('admin/', admin.site.urls),
    url(r'^api/', include(('core.urls', 'core'), namespace='core')),
    url(r'^api_web_token_auth/', obtain_jwt_token),
]

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)